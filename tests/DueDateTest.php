<?php


use emarsys\DueDate;
use PHPUnit\Framework\TestCase;

class DueDateTest extends TestCase
{

    protected $dueDate;

    public function setUp() : void
    {
        $this->dueDate = new DueDate();
    }

    /**
     * Test whether same day calculation works
     */
    public function testDueOnSameDay() {
        $submitDate = new \DateTime("2020-05-04 9:05:00");
        $turnaroundTime = 4;
        $result = $this->dueDate->calculateDueDate($submitDate, $turnaroundTime);

        $submitDate->modify("+4 hours");

        $this->assertEquals($result, $submitDate);
    }

    /**
     * Test whether next day calculation works
     */
    public function testDueOnNextDay() {
        $submitDate = new \DateTime("2020-05-04 9:05:00");
        $turnaroundTime = 8;
        $result = $this->dueDate->calculateDueDate($submitDate, $turnaroundTime);

        $submitDate->modify("+24 hours");

        $this->assertEquals($result, $submitDate);
    }

    /**
     * Test whether next day calculation works
     */
    public function testDueOnNextDay2() {
        $submitDate = new \DateTime("2020-05-04 9:05:00");
        $turnaroundTime = 9;
        $result = $this->dueDate->calculateDueDate($submitDate, $turnaroundTime);

        $submitDate->modify("+25 hours");

        $this->assertEquals($result, $submitDate);
    }

    /**
     * Test whether Friday-Monday "jump" works
     */
    public function testDueOnNextMonday() {
        $submitDate = new \DateTime("2020-05-01 9:05:00"); // Friday
        $turnaroundTime = 8;
        $result = $this->dueDate->calculateDueDate($submitDate, $turnaroundTime);

        $submitDate->modify("+3 days");

        $this->assertEquals($result, $submitDate);
    }

    /**
     * Test whether calculations works between months
     */
    public function testDueOnNextMonth() {
        $submitDate = new \DateTime("2020-05-01 9:05:00");
        $turnaroundTime = 8 * 21;
        $result = $this->dueDate->calculateDueDate($submitDate, $turnaroundTime);

        $submitDate->modify("+1 month");

        $this->assertEquals($result, $submitDate);
    }

}
<?php


namespace emarsys;


use DateTime;

class DueDate
{

    // When the workhours start at on workdays (9. AM)
    const DAY_STARTS_AT = 9;
    // How many workhours on a workday (8 hour per workday)
    const HOURS_PER_DAY = 8;

    /**
     * Calculates due date for an issue
     *
     * Workhours calculated only for workdays (Monday - Friday)
     * from @see DueDate::DAY_STARTS_AT hour for the next @see DueDate::HOURS_PER_DAY hours
     *
     * @param DateTime $submitDate start date when the issue is submitted, can only contain a value between working hours
     * @param int $turnaroundTime turnaround time in hours
     * @return DateTime when then issue is due
     */
    public function calculateDueDate(DateTime $submitDate, int $turnaroundTime)
    {
        $dayOfWeek      = (int) $submitDate->format("w"); // which day of the week the issue is submitted
        $minuteOfDay    = (int) $submitDate->format("H") * 60 + (int) $submitDate->format("m"); // time of issue submission
        $remainingTime  = $turnaroundTime * 60; // remaining time until the due date - in minutes
        $minutes        = 0; // how much minutes do we need to add to the submit date
        $days           = 0; // how much days do we need to add to the submit date

        // Count workhours for every day until there is no more time left
        while ($remainingTime > 0) {
            $timeLeftForCurrentDay = (self::DAY_STARTS_AT * 60 + self::HOURS_PER_DAY * 60) - $minuteOfDay;
            if ($remainingTime < $timeLeftForCurrentDay) {
                $minutes += $remainingTime;
                $remainingTime = 0;
            } else {
                $remainingTime -= $timeLeftForCurrentDay;
                $minutes += $timeLeftForCurrentDay + (24 - static::HOURS_PER_DAY) * 60;

                // go to next day
                $dayOfWeek++;
                $minuteOfDay = static::DAY_STARTS_AT * 60;
                if ($dayOfWeek > 5) {
                    // do not calculate for Saturday/Sunday but add as a full day then go to Monday
                    $days+=2;
                    $dayOfWeek = 1;
                }
            }
        }

        // Add calculated values to submit date
        $dueDate        = clone $submitDate;
        $dueDate->modify("+${days} days +${minutes} minute");

        return $dueDate;
    }

}